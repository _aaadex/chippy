# Chippy

A Chip 8 interpreter made with C that uses raylib for graphics and inputs.

### Some screenshots
![Screenshot-1](imgs/1.png)
<br>
![Screenshot-2](imgs/2.PNG)
<br>
![Screenshot-3](imgs/3.PNG)
<br>
![Screenshot-4](imgs/num-4.PNG)

<p> To build: </p>
First clone the repository and cd into the directory...
<br>
make
<br>
To run: 
<br>
cd bin && ./chippy assets/roms/THE-GAME.ch8

### Some articles that were helpful
[Cowgod's Technical Chip 8 Reference](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
<br>
[Guide to Making a Chip 8 Interpreter](https://tobiasvl.github.io/blog/write-a-chip-8-emulator/)
